import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HighchartComponent } from './modules/highchart/component/highchart.component';
import { DashboardComponent } from './modules/dashboard/component/dashboard.component';
import { DonutchartComponent } from './modules/donutchart/component/donutchart.component';
import { ProjectTimelineComponent } from './modules/project-timeline/project-timeline.component'
const routes: Routes = [
  { path: 'high', component: HighchartComponent },
  { path: 'do', component: DonutchartComponent },
  { path: 'timeline', component: ProjectTimelineComponent },
  { path: '', component: DashboardComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
