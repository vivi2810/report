import { Component, OnChanges, Input, EventEmitter, Output } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  animations: [
    trigger('openBackDrop' , [
      state('open', style({
        width: '100%',
        background: '#0000002b'
      })),
      state('close', style({
        width: 0,
        background: 'none'
      })),
      transition('open => close', [
        animate(0, style({
          background: 'none'
        })),
        animate('0.5s',
          style ({ transform: 'translateX(100%)' }),
        )
      ])
    ]),
    trigger('openSidebar', [
      state('sidebarOpen', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(100%)' }),
        animate('0.1s 200ms ease-in')
      ]),
      transition(':leave', [
      ])
    ])
  ]
})
export class SidebarComponent implements OnChanges {
  @Input () isModalOpen = false;
  @Input () title = '';
  @Input () scrollToBottom = false;
  @Input () hideFooter = false;
  @Output() public closeModal: EventEmitter<any> = new EventEmitter();
  constructor() {}
  ngOnChanges() {
    if (this.scrollToBottom && this.isModalOpen) {
      setTimeout(() => {
        const scrollContent: HTMLElement = document.getElementById('scroll-content');
        scrollContent.scrollTop = scrollContent.scrollHeight;
      });
    }
  }
  close(): void {
    this.closeModal.emit();
  }
}
