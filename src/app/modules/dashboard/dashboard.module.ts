import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './component/dashboard.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { HighchartModule } from '../highchart/highchart.module';
import { DonutchartModule } from '../donutchart/donutchart.module';
import { PiechartModule } from '../piechart/piechart.module';
import { ProjectDetailsModule } from '../project-details/project-details.module';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    SharedModule,
    HighchartModule,
    DonutchartModule,
    PiechartModule,
    ProjectDetailsModule
  ]
})
export class DashboardModule { }
