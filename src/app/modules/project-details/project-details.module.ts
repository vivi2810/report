import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectDetailsComponent } from './component/project-details.component';

@NgModule({
  declarations: [ProjectDetailsComponent],
  imports: [
    CommonModule
  ],
  exports: [ProjectDetailsComponent]
})
export class ProjectDetailsModule { }
