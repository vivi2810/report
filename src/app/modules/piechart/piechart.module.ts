import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { PiechartComponent } from './component/piechart.component';

@NgModule({
  declarations: [PiechartComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [PiechartComponent]
})
export class PiechartModule { }
