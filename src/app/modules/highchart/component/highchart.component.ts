import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-highchart',
  templateUrl: './highchart.component.html',
  styleUrls: ['./highchart.component.scss']
})
export class HighchartComponent implements OnInit {

  @Output() ModalTrigger = new EventEmitter;

  constructor() {}

  highcharts = Highcharts;
  chartOptions = {
    chart: {
       type: 'column'
    },
    credits: false,
    title: {
       text: ''
    },
    legend: {
      align: 'right',
      verticalAlign: 'top',
      floating: true,
      x: 0,
      y: 30
   },
   colors: ['#0D4A52', '#0FADC5'],
    xAxis: {
       categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul',
       'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    },
    yAxis : {
       min: 0,
       title: {
          text: ''
       },
       visible: false,
       gridLineWidth: 0,
       minorGridLineWidth: 0,
    },
    plotOptions : {
       column: {
          pointPadding: 0,
          borderWidth: 0
       },
       series: {
        events: {
          click: (event) => this.ModalTrigger.emit(event)
        },
      },
    },
    series: [{
       name: 'Tokyo',
       data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6,
          148.5, 116.4, 194.1, 95.6, 54.4]
    },
    {
       name: 'Berlin',
       data: [42.4, 33.2, 84.5, 99.7, 112.6, 155.5, 157.4, 160.4,
          87.6, 139.1, 66.8, 51.1]
    }]
 };

  ngOnInit() {}
}
