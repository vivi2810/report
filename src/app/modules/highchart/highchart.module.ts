import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighchartComponent } from './component/highchart.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [HighchartComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [HighchartComponent]
})
export class HighchartModule { }
