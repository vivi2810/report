import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DonutchartComponent } from './component/donutchart.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [DonutchartComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [DonutchartComponent]
})
export class DonutchartModule { }
