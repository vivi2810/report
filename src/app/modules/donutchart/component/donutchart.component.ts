import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-donutchart',
  templateUrl: './donutchart.component.html',
  styleUrls: ['./donutchart.component.scss']
})
export class DonutchartComponent implements OnInit {

  highcharts = Highcharts;
   chartOptions = {
      chart : {
         plotBorderWidth: null,
         plotShadow: false
      },
      colors: ['#A1C522', '#6A851E', '#263015'],
      credits: false,
      title : {
         text: ''
      },
      // tooltip : {
      //    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      // },
      plotOptions : {
         pie: {
            shadow: false,
            center: ['50%', '50%'],
            size: '80%',
            innerSize: '25%',
            dataLabels: {
              enabled: false
            },
            showInLegend: true
         }
      },
      series : [{
         type: 'pie',
         name: 'Browser share',
         data: [
            ['Firefox',   45.0],
            ['IE',       26.8],
            {
               name: 'Chrome',
               y: 12.8,
              //  sliced: true,
              //  selected: true
            },
            // ['Safari',    8.5],
            // ['Opera',     6.2],
            // ['Others',      0.7]
         ]
      }]
   };

  constructor() { }

  ngOnInit() {
  }

}
