import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  imagePath: string;

  constructor() { }

  ngOnInit() {
    this.imagePath = 'assets/images/logo@1x.png';
  }

}
