import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './layouts/components/navbar/navbar.component';
import { AppLayoutComponent } from './layouts/components/app-layout/app-layout.component';
import { SharedModule } from './shared/shared.module';
import { ApplicationProperties } from './constants/app.properties';

import { HighchartModule } from './modules/highchart/highchart.module';
import { DashboardModule } from './modules/dashboard/dashboard.module';
import { DonutchartModule } from './modules/donutchart/donutchart.module';
import { PiechartModule } from './modules/piechart/piechart.module';
import { ProjectDetailsModule } from './modules/project-details/project-details.module';
import { ProjectTimelineComponent } from './modules/project-timeline/project-timeline.component';

@NgModule({
  declarations: [AppComponent, AppLayoutComponent, NavbarComponent, ProjectTimelineComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    AppRoutingModule,
    HighchartModule,
    DashboardModule,
    DonutchartModule,
    BrowserAnimationsModule,
    PiechartModule,
    ProjectDetailsModule
  ],
  providers: [ApplicationProperties],
  bootstrap: [AppComponent]
})
export class AppModule {}
